//Aplicacion especifico para API
require("dotenv").config();
var express = require("express");
var api = express();
var server = require("http").createServer(api);
var mongoose = require("mongoose");

server.listen(process.env.API_PORT,(err,res)=>{
    if(err) console.log("Error en el servidor"+err);
    console.log("API conectado");
})

mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true},
    (err, res) => {
        if (err) console.log(`ERROR: connecting to Database: ${err}`);
        else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);

var apiRoutes = require(__dirname+'/app/routes/api');

api.use("/",apiRoutes);
