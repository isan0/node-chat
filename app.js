require("dotenv").config();
//const { Socket } = require("dgram");
var express = require("express");
var app = express();
var server = require("http").createServer(app);
var mongoose = require("mongoose");
var io = require("socket.io")(server)
var numUsers = 0;

server.listen(process.env.SERVER_PORT, (err, res) => {
    if (err) console.log("Error en el servidor" + err);
    console.log("Servidor conectado");
})

mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
        if (err) console.log(`ERROR: connecting to Database: ${err}`);
        else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
);

app.use(express.static(__dirname + "/public"));
app.set("views", __dirname + "/views");
app.set("view engine", "pug");

var chatRoutes = require(__dirname + "/app/routes/chat");
var formRoutes = require(__dirname + "/app/routes/formulario");
var incRoutes = require(__dirname + "/app/routes/incidencias");
var apiRoutes = require(__dirname + "/app/routes/api");

app.route('/').get((req, res, next) => {
    res.render("index")
})

app.use("/chat", chatRoutes);

app.use("/formulario", formRoutes);

app.use("/incidencias", incRoutes);

app.use("/api", apiRoutes);

io.on("connection", (socket) => {
    console.log("Socket conectado");
    socket.user = "Usuario " + numUsers++;
    socket.join(socket.user);
    //Permite conexion con todos
    socket.on("newMSG", (data) => {
        data["user"] = socket.user;
        if (data["room"] == "all") {
            socket.broadcast.emit("newMSG", data);
        } else {
            socket.to(data["room"]).emit("newMSG", data);
        }
    })
    socket.on("newUser", (data) => {
        data["user"] = socket.user;
        socket.broadcast.emit("newUser", data);
    })
})