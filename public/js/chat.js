$(document).ready(function () {
    const socket = io();
    $("#chat").submit(function (e) {
        e.preventDefault();
        var msg = $("#msg").val();
        $("#chatBox").append(`<p>${msg}<p>`);
        console.log($("#room").val());
        var toSend = { room: $("#room").val(), text: msg }
        socket.emit("newMSG", toSend)
    })
    socket.on("newMSG", (data) => {
        //Falta funcion de mostrar en cuadro
        //console.log(data)
        var head = ""
        if (data["room"] == "all") {
            head = "<b style='color:red'>[All]</b>"
        }
        $("#chatBox").append(`<p style="text-align: right"><b>${head}${data["user"]}:</b> ${data["text"]}<p>`);

    })
})