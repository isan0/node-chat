var moongose = require("mongoose"),
    Formulario = require("../models/Formulario");

exports.load=async()=>{
    var res= await Formulario.find({})
    return res;
}
exports.save = (req)=>{
    var newForm = new Formulario(req);
    newForm.save((err,res)=>{
        if(err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}