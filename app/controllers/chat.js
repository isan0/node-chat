var moongose = require("mongoose"),
    Chat = require("../models/Chat");

exports.load =async()=>{
    var res=await Chat.find({})
    return res;
}
exports.save = (req)=>{
    var newChat = new Chat(req);
    newChat.save((err,res)=>{
        if(err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}