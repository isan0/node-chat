var mongoose = require("mongoose"),
    Schema = mongoose.Schema

    var formSchema = new Schema({
        nombre: {type: String},
        dni: {type:String},
        incidencia: {type:[Object]},
        urgencia: {type:[Object]},
        descripcion: {type:String}
    });

module.exports = mongoose.model("Formulario",formSchema);