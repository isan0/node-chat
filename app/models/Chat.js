var mongoose = require("mongoose"),
    Schema = mongoose.Schema

    var chatSchema = new Schema({
        user1: {type: String},
        user2: {type:String},
        msg: {type:[Object]},
        create_at: {type:Date,default:Date.new},
    });

module.exports = mongoose.model("Chat",chatSchema);