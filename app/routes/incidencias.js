var express = require("express");
router = express.Router();
var IncCntr = require(__dirname+"/../controllers/incidencias");

router.route('/').get(async(req,res,next)=>{
    var result = await IncCntr.load();
    console.log(result);
    res.render("incidencias",result);
});

module.exports = router;