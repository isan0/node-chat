var express = require("express");
router = express.Router();
var chatCntr = require(__dirname+"/../controllers/chat");

//Rellenar con todas las url que acepte la API
router.route("/chat").get(async(req,res,next)=>{
    var result = await chatCntr.load();
    res.status(200).jsonp(result);
});


module.exports = router;