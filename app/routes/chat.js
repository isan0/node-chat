var express = require("express");
router = express.Router();
var chatCntr = require(__dirname + "/../controllers/chat");

router.route("/").get(async (req, res, next) => {
    var result = await chatCntr.load();
    res.render("chat", result[0]);
});

router.route("/agente").get(async (req, res, next) => {
    var result = await chatCntr.load();
    res.render("chat", { agente: true });
});

router.route('/new').get((req, res, next) => {
    var newChat = {
        user1: '/user/1',
        user2: 'user/2',
        msg: [
            {
                user: 1,
                text: "hola que tal"
            },
            {
                user: 2,
                text: "todo bien gracias"
            }
        ]
    };
    chatCntr.save(newChat);
    res.send("Guardado");
})
module.exports = router;