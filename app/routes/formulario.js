var express = require("express");
router = express.Router();
var FormCntr = require(__dirname+"/../controllers/formulario");

router.route('/').get(async(req,res,next)=>{
    var result = await FormCntr.load();
    console.log(result);
    res.render("formulario",result);
});

router.route('/new').get((req,res,next)=>{
    var newForm = {
        nombre: 'Juan',
        dni: '12345678P',
        incidencia: [1],
        urgencia:['Alta'],
        descripcion: 'peto el ordenador'
    };
    FormCntr.save(newForm);
    res.send("Guardado");
})

module.exports = router;